Jason Yang Lab 3

Banker Lab (Lab 3) for Operating Systems Spring 2016
1 source code file(s) are part of this submission:
        -> banker.cpp


To compile the lab, execute:
$ g++ -std=gnu++0x banker.cpp

To run the lab, execute:
$ ./a.out input_#.txt


The code was tested on mauler.cims.nyu.edu.


1000 character limit
C++

