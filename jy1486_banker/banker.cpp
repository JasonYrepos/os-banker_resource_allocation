//Jason Yang
#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif
#include <iostream>
#include <algorithm>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <cstring>
#include <math.h>
#include <fstream>
#include <deque>
#include <vector>
using namespace std;

class process;
class acts;
void printall(vector<process> myplist);
void fifo(vector<process> argprocess,vector<vector<int> > AlloRes,vector<int> Avresource);
void banker(vector<process> argprocess,vector<vector<int> > AlloRes,vector<int> Avresource);
bool deadlocked(vector<process> ,vector<int>);
bool tryalloc(process &proccarg,vector<int> &avail, vector< vector<int> > &allocc);
bool bankit(process &targ,int &reqproc, vector<process> &plistarg,vector<int> &availarg,vector<vector <int> > &needarg, vector<vector <int> > &alloarg, vector< vector <int> > &prequestarg  );
void dangerfunction(bool &dangerarg,vector<process> &myprocesslistarg,vector<int> &availablearg,vector<vector <int> > &allocatedarg,deque<process> &blockedarg);
void blockedcheck(deque <process> &blockedarg,process &popitarg,vector<process> &blocktordyarg,vector<int> &availablearg,vector<vector <int> > &allocatedarg,int &blockreqarg,vector<process> &waitarg,int &activityarg);

//Class acts holds data on each procedure that each tasks have given from input
//The procedures contains the type of procedure, the task number, the delay, the resource type and the units of resources
class acts{
public:
string id;
int nnum;
int delay;
int restype;
int resn;
bool firsttime;
acts(string oid,int nnum2, int odelay, int orestype, int oresn){
    nnum = nnum2;
    id = oid;
    delay = odelay;
    restype = orestype;
    resn = oresn;

}

};
//Class process holds an list of acts which will be looped through
//Contains a pointer that will move as each acts in the actlist are completed
//Contains how long it is blocked and when it finished and if it is aborted or not
class process{
public:

bool indelay ;
vector<acts> actlist;
int left ;
int instpointer ;
int blockednum ;
int fintime ;
bool aborted;
int iden;
//Constructor of a process
process(int iden1){
iden = iden1;
indelay = false;
left = 0;
instpointer = 0;
blockednum = 0;
aborted = false;
fintime = 0;
}
//Checks if process is already finished
bool isfin(){
    if(fintime > 0){
        return true;
    }
    return false;
}
//Returns the current method the task needs to run
string returnCur(){
return actlist[instpointer].id;
}
//Returns an act object which holds data on the procedure it needs to run
acts returnact(){
return actlist[instpointer];
}
//Equivalent overload for finding algorithm
bool operator== (   const process &p2){
return iden == p2.iden;
}
// Checks for tasks that are going to terminate without computing
bool fined(){

if(actlist[instpointer].id == "terminate" && left == 0 ){
    return true;
}

return false;
}
};
int main(int argc, char* argv[]){
    vector<int> Aresource;
    vector<vector<int> > AllocResource;
    vector<acts> itotalactlist;
    string inputd;
    string line;
    string filename = argv[1];
    ifstream rfile;
    vector <string> inpu;
    vector <string> inpu2;
    vector <vector<string> > inpe;
    int a = 0;
    int totalprocess = 0;
    int totalresource = 0;
    vector<process> processlist;
    rfile.open(filename.c_str());
    while(rfile.good()){
    getline(rfile, line, '\n');

    if(line.size() != 0){

    inpu.push_back(line);
    }
    }

    char destring[1000];
    // reads the first line and scrape for data about total resources and tasks and units for each resource type
    strcpy(destring,inpu[0].c_str());
    char *eee = strtok(destring, " ");
    int fff = 0;
    while (eee){
        if(fff == 0){
            totalprocess = atoi(eee);
        }else if(fff == 1){
            totalresource = atoi(eee);
        }else{
            Aresource.push_back(atoi(eee));
        }
        fff++;
        eee = strtok(NULL, " ");
    }
	int *available;
	available = new int[totalresource];

	int **allocated;
	allocated = new int*[totalprocess];
	for (int i = 0; i < totalprocess; i++) {
		allocated[i] = new int[totalresource];
	}
    //Sets up matrix of empty allocated resources of each prcoess
    for(int i = 0; i < totalprocess; i ++){
            vector<int> teeemp;
            for(int j =0; j < totalresource; j++){
                teeemp.push_back(0);
            }
            AllocResource.push_back(teeemp);

    }
    //Parse the rest of file and puts data into a act list or list of procedures
    for(int i = 1;i< inpu.size(); i++){
            char curstring[1000];
            strcpy(curstring,inpu[i].c_str());
            char *p = strtok(curstring, " ");
            string qid;
            int gnum;
            int qdelay;
            int qrestype;
            int qresn;
            int g = 0;
            while (p) {

            if(g == 0){
                qid = p;
            }else if(g == 1){
                gnum = atoi(p);
            }else if(g == 2){
                qdelay = atoi(p);
            }else if(g == 3){
                qrestype = atoi(p);
            }else if(g == 4){
                qresn = atoi(p);

            }
            if(g == 4){

            itotalactlist.push_back(acts(qid,gnum,qdelay,qrestype,qresn));
            }

            p = strtok(NULL, " ");
            g++;
            }



    }
rfile.close();

vector<int> processid1;
// Use actlist to divide procedure list to its respective tasks
int jjj = 0;
for(int i = 0; i < itotalactlist.size(); i++){
        if(itotalactlist[i].id == "initiate"  ){
            bool alreadyinit = false;
                for(int j =0; j < processlist.size(); j++){
                if(processlist[j].iden == itotalactlist[i].nnum){
                    processlist[j].actlist.push_back(itotalactlist[i]);
                    alreadyinit = true;
                }
            }
            if(alreadyinit == false){
            process processtmp = process(i);
            processtmp.iden = itotalactlist[i].nnum;
            processtmp.actlist.push_back(itotalactlist[i]);
            processlist.push_back(processtmp);
            }

        }else{
            for(int j =0; j < processlist.size(); j++){
                if(processlist[j].iden == itotalactlist[i].nnum){
                    processlist[j].actlist.push_back(itotalactlist[i]);
                }
            }

        }

}
cout << "\t\tFIFO\n";
fifo(processlist, AllocResource,Aresource);
cout << "\n\n\t\tBANKER's";
banker(processlist, AllocResource,Aresource);
cout <<'\n';


}
//FIFO Method ,aborts if in danger and releases resources to continue
void fifo(vector<process> argprocess,vector<vector<int> > AlloRes,vector<int> Avresource){
    vector<process> myprocesslist = argprocess; // holds the processes that will have to finish its list of acts or abort
    vector<int> available = Avresource; //holds the number of available resources
    vector<int> delayed;

    for(int i = 0; i < argprocess.size(); i++){
        delayed.push_back(0);
    }

    vector<vector <int> > allocated = AlloRes; //holds the resource of each task that is allocated, initially filled with 0
int cycle = 0;
deque<process> blocked; //list of blocked tasks due to not having enough available resources for request
vector<process> blocktordy;
vector<process> wait;
bool danger = false; //flag if the state is going to dead lock

for(;;){

    int activity = 0;
    int blockreq = 0;
    process popit(-1);

    wait.clear();
  vector<int> releasedres;

    //released resource to be refilled at end of loop
    for(int i = 0; i < available.size(); i++){
      releasedres.push_back(0);
    }
    cycle++;

    //Checks if in danger and if it is, process aborts and available vectors has more resources and the loop can continue by moving
    //out the blocked process.
    dangerfunction(danger,myprocesslist,available,allocated,blocked);
    //Checks if the blocklist if it is possible to allocate resources and it is it will be allowed to continue its instructions
    //otherwise it will remain in the blockedlist
    blockedcheck(blocked,popit,blocktordy,available,allocated,blockreq,wait,activity);

    //main loop through processes, and sees if it is not blocked or aborted
    for(int i = 0; i < myprocesslist.size(); i++){
        //will not loop through processes that are currently blocked
        if(  find(blocked.begin(),blocked.end(),myprocesslist[i])== blocked.end() && find(blocktordy.begin(),blocktordy.end(),myprocesslist[i])== blocktordy.end()){
            //computing step if when there is a delay
            if(myprocesslist[i].left != 0 ){
             activity++;
                myprocesslist[i].left--;

                if( myprocesslist[i].returnCur() == "terminate" && myprocesslist[i].aborted == false && myprocesslist[i].left == 0){
                        myprocesslist[i].fintime = cycle;
                    }
            }else{
            //If process has delay, it will need to be computed and it will skip other instructions until computation is finished
                if(myprocesslist[i].returnact().delay > 0 && myprocesslist[i].indelay == false){

                    activity++;
                    myprocesslist[i].indelay = true;
                    myprocesslist[i].left = myprocesslist[i].returnact().delay ;
                    myprocesslist[i].left--;

                    if(myprocesslist[i].fined() && myprocesslist[i].left == 0){
                        myprocesslist[i].fintime = cycle;
                    }
                }
            //initiate step, moves pointer to next instruction
                else if(myprocesslist[i].returnact().id == "initiate"){

                    activity++;
                    myprocesslist[i].instpointer++;
                    myprocesslist[i].indelay = false;
                }
            //request step, checks if it is possible to allocate resource for process, if not it will be blocked
                else if(myprocesslist[i].returnact().id == "request"){

                    activity++;

                    if(tryalloc(myprocesslist[i],available,allocated)){

                    }else{

                        blockreq++;
                        blocked.push_back(myprocesslist[i]);
                    }
                    myprocesslist[i].indelay = false;
                }
            //release step, remove allocated resources and available resources increase
            //goes to termination instruction and checks if termination is delayed or not without needing to recycle
                else if(myprocesslist[i].returnact().id == "release"){

                    activity++;
                    releasedres[myprocesslist[i].returnact().restype -1] += myprocesslist[i].returnact().resn;
                    allocated[i][myprocesslist[i].returnact().restype -1] -= myprocesslist[i].returnact().resn;
                    myprocesslist[i].instpointer++;

                    myprocesslist[i].left = myprocesslist[i].returnact().delay;
                    myprocesslist[i].indelay = false;

                    if( myprocesslist[i].returnact().id == "terminate" && myprocesslist[i].aborted == false && myprocesslist[i].left == 0){

                        myprocesslist[i].fintime = cycle;
                        myprocesslist[i].indelay = false;
                    }
                }


            }

            }
        }


    //Available regains resource at end of processing loop

    for(int i =0; i < available.size();i++){
        available[i] += releasedres[i];

    }
    //Checks and copies modified blocked process into processlist
for(int i = 0; i < myprocesslist.size(); i++){
    for(int j = 0; j < blocktordy.size(); j++){
        if(myprocesslist[i].iden == blocktordy[j].iden){
            myprocesslist[i] = blocktordy[j];

        }
    }

}
    blocktordy.clear();
    //Check if state is in danger depending if attempted activity is blocked
    if(activity == blockreq){
        danger = true;
    }else{
        danger = false;

    }
    //breaks loop when all processes are finished
    bool shouldend = true;
    for(int i = 0; i< myprocesslist.size();i++){
        if(!myprocesslist[i].fined()){

            shouldend = false;
        }
    }
    if(shouldend == true){
        break;
    }

}


printall(myprocesslist);
}
//Checks if in danger and if it is, process aborts and available vectors has more resources and the loop can continue by moving
//out the blocked process.
void dangerfunction(bool &dangerarg,vector<process> &myprocesslistarg,vector<int> &availablearg,vector<vector <int> > &allocatedarg,deque<process> &blockedarg){
if(dangerarg){
        //if it is in danger then the processlist will be looped and abort each task until available resource is enough
        for(int i = 0; i < myprocesslistarg.size(); i++){
            if(!myprocesslistarg[i].fined() && myprocesslistarg[i].aborted == false ){
                myprocesslistarg[i].aborted = true;
                myprocesslistarg[i].instpointer = myprocesslistarg[i].actlist.size() -1;
                myprocesslistarg[i].blockednum = 0;

            for(int j = 0; j < availablearg.size();j++){
                availablearg[j] += allocatedarg[i][j];
            }

            blockedarg.erase(remove(blockedarg.begin(),blockedarg.end(),myprocesslistarg[i]),blockedarg.end());
        //if there is enough resource to continue, it will stop aborting tasks
            if(!deadlocked(myprocesslistarg,availablearg)){
                dangerarg = false;
                break;
            }
            }
        }
    }

}
//Checks if the blocklist if it is possible to allocate resources and it is it will be allowed to continue its instructions
//otherwise it will remain in the blockedlist
void blockedcheck(deque <process> &blockedarg,process &popitarg,vector<process> &blocktordyarg,vector<int> &availablearg,vector<vector <int> > &allocatedarg,int &blockreqarg,vector<process> &waitarg,int &activityarg){

    while(!blockedarg.empty()){

        popitarg = blockedarg.front();
        blockedarg.pop_front();
        activityarg++;
        //if the request can be made to allocate then it will be removed from the block state
        if(tryalloc(popitarg,availablearg,allocatedarg)){

            blocktordyarg.push_back(popitarg);
        }else{

            blockreqarg++;
            waitarg.push_back(popitarg);
        }
    }
    for(int e = 0; e < waitarg.size(); e++){
        blockedarg.push_back(waitarg[e]);
    }
}
//Checks if available resources is satisfactory for requested resources or else it will mean a deadlock
bool deadlocked(vector<process> processlistarg,vector<int> avail){
for(int i = 0; i< processlistarg.size(); i++){
    if(!processlistarg[i].aborted && !processlistarg[i].fined()){
        if(avail[(processlistarg[i].returnact().restype)-1] >= processlistarg[i].returnact().resn){
            return false;
        }
    }
}
return true;
}
//attempts to allocate resources and then goes to next instruction but if fails, it will remain or will block
bool tryalloc(process &proccarg,vector<int> &avail, vector< vector<int> > &allocc){

    if(avail[proccarg.returnact().restype-1] - proccarg.returnact().resn  < 0){
        proccarg.blockednum++;
        return false;
    }else{

        avail[proccarg.returnact().restype-1] -= proccarg.returnact().resn;
        allocc[proccarg.returnact().nnum - 1][proccarg.returnact().restype-1] += proccarg.returnact().resn;
        proccarg.instpointer++;
        return true;
    }

}
//Prints out the results
void printall(vector<process> myplist){
int tott = 0;
int totb = 0;
for(int i = 0; i < myplist.size(); i++){
    tott = tott + myplist[i].fintime;
    totb = totb + myplist[i].blockednum;
    int percent = round( ((double)(myplist[i].blockednum)/(double)(myplist[i].fintime)) * 100);
    cout << "Task " << i + 1<< "\t";
    if(myplist[i].aborted == true){
    cout << "aborted";
    cout << '\n';
    }else{
    cout << myplist[i].fintime << "\t";
    cout << myplist[i].blockednum << "\t";
    cout << percent << "%";
    cout << '\n';
}



}
int tpercent = round( ((double)(totb)/(double)(tott)) * 100);
cout << "total\t";
    cout << tott << "\t";
    cout << totb << "\t";
    cout << tpercent << "%\t";
}
//-----------------------------------------------------------------------------------------------------------------------------
//Banker method will block processes when in a unsafe state and unblock processes when in a safe state without deadlocking
void banker(vector<process> argprocess,vector<vector<int> > AlloRes,vector<int> Avresource){

    vector<process> myprocesslist = argprocess; //process list that will loop
    vector <int> available = Avresource; // available resources
    vector<vector <int> > Maxn = AlloRes; //what resources each processes each initially claimed to need
    vector<vector <int> > allocated = AlloRes; //what is already allocated for each process
    vector<vector <int> > need = AlloRes; // what is needed according to current state of process
    vector<vector <int> > prequest = AlloRes; // process requested resource
    int requestprocess;


int cycle = 0;
deque<process> blocked; //blocked processes are held here if in a unsafe state
vector<process> blocktordy;
vector<process> wait;
bool danger = false;

for(;;){


    process popit(-1);

    wait.clear();
    //released resources,need and allocate are given back at the end of loop
  vector<int> releasedres;
  vector< vector <int> > releasedneed;
  vector< vector <int> > releasedallocate;

    for(int i = 0; i < myprocesslist.size(); i++){
            vector <int> temp;
        for(int j = 0; j < available.size(); j++){
            temp.push_back(0);

        }
            releasedneed.push_back(temp);
            releasedallocate.push_back(temp);
    }
    for(int i = 0; i < available.size(); i++){
      releasedres.push_back(0);
    }
    cycle++;


    //checks if the blocked list isn't empty, then sees of process is in a safe state by temporarily checking resource allocation
    while(blocked.empty() == false){

        popit = blocked.front();
        blocked.pop_front();
        //banker algorithm checks if process is in a safe state or not by checking if request in allocation allows continuation
        if(bankit(popit,requestprocess, myprocesslist,available,need, allocated, prequest )){

            blocktordy.push_back(popit);
        }else{
            //not safe so it needs to continue to wait

            wait.push_back(popit);
        }
    }
    for(int e = 0; e < wait.size(); e++){
        blocked.push_back(wait[e]);
    }
    //main loop through processes, and sees if it is not blocked or aborted
    for(int i = 0; i < myprocesslist.size(); i++){


        if(!myprocesslist[i].isfin() && find(blocked.begin(),blocked.end(),myprocesslist[i])== blocked.end() && find(blocktordy.begin(),blocktordy.end(),myprocesslist[i])== blocktordy.end()){
        //computing step if when there is a delay
            if(myprocesslist[i].left != 0 ){
                     myprocesslist[i].left--;
                if( myprocesslist[i].returnCur() == "terminate" && myprocesslist[i].aborted == false && myprocesslist[i].left == 0){
                        myprocesslist[i].fintime = cycle;
                        myprocesslist[i].indelay == true;
                    }
            }else{

        //If process has a delay, it will need to compute
                if(myprocesslist[i].returnact().delay > 0 && myprocesslist[i].indelay == false){


                    myprocesslist[i].indelay = true;
                    myprocesslist[i].left = myprocesslist[i].returnact().delay ;
                    myprocesslist[i].left--;

                    if(myprocesslist[i].fined() && myprocesslist[i].left == 0){
                        myprocesslist[i].fintime = cycle;
                            myprocesslist[i].indelay == false;
                    }
                }
        //Checks if claim is more than allowed resulting in aborted otherwise it checks what the process needs and total it can have
                else if(myprocesslist[i].returnact().id == "initiate"){

                    if(myprocesslist[i].returnact().resn > available[myprocesslist[i].returnact().restype - 1]){
                        myprocesslist[myprocesslist[i].returnact().nnum - 1].aborted = true;
                        myprocesslist[myprocesslist[i].returnact().nnum - 1].instpointer = myprocesslist[myprocesslist[i].returnact().nnum - 1].actlist.size() - 1;
                        myprocesslist[myprocesslist[i].returnact().nnum - 1].blockednum = 0;

                    }else{
                        Maxn[myprocesslist[i].returnact().nnum - 1][myprocesslist[i].returnact().restype - 1] = myprocesslist[i].returnact().resn;
                        need[myprocesslist[i].returnact().nnum - 1][myprocesslist[i].returnact().restype - 1] = myprocesslist[i].returnact().resn;
                        myprocesslist[i].instpointer++;
                    }
                myprocesslist[i].indelay = false;

                }
                //Request checks using banker algorithm if hypothetical allocation results in a safe state
                else if(myprocesslist[i].returnact().id == "request"){



                    if(bankit(myprocesslist[i],requestprocess, myprocesslist,available,need, allocated, prequest )){


                    }else{


                        blocked.push_back(myprocesslist[i]);
                    }
                     myprocesslist[i].indelay = false;
                }
                //releases resources allocated to available,need and alocate and terminates if there is no delay
                else if(myprocesslist[i].returnact().id == "release"){


                    releasedres[myprocesslist[i].returnact().restype -1] += myprocesslist[i].returnact().resn;
                    releasedallocate[myprocesslist[i].returnact().nnum -1][myprocesslist[i].returnact().restype -1] = myprocesslist[i].returnact().resn;
                    releasedneed[myprocesslist[i].returnact().nnum -1][myprocesslist[i].returnact().restype -1] = myprocesslist[i].returnact().resn;


                    myprocesslist[i].instpointer++;
                    myprocesslist[i].left = myprocesslist[i].returnact().delay;
                    myprocesslist[i].indelay = false;

                    if(myprocesslist[i].returnact().id == "terminate" && myprocesslist[i].aborted == false && myprocesslist[i].left == 0){

                        myprocesslist[i].fintime = cycle;
                        myprocesslist[i].indelay = false;
                    }
                }

            }
        }

    }

//gives back available,need and allocated resources that were released in loop
    for(int i =0; i < available.size();i++){
        available[i] += releasedres[i];

    }
    for(int i =0; i < myprocesslist.size(); i++){
        for(int j = 0; j < available.size(); j++){
            allocated[i][j] = allocated[i][j] - releasedallocate[i][j];
            need[i][j] = need[i][j] + releasedneed[i][j];
        }
    }
// blocked processes is merged back with processlist
for(int i = 0; i < myprocesslist.size(); i++){
    for(int j = 0; j < blocktordy.size(); j++){
        if(myprocesslist[i].iden == blocktordy[j].iden){
            myprocesslist[i] = blocktordy[j];

        }
    }

}

    blocktordy.clear();
//if all process ends then finishes
bool shouldend = true;
    for(int i = 0; i< myprocesslist.size();i++){
        if(!myprocesslist[i].fined()){

            shouldend = false;
        }
    }
    if(shouldend == true){
        break;
    }


}

cout << '\n';
printall(myprocesslist);

}
//banker algorithm checks hypothetical allocation to see if it results in a safe or unsafe state
bool bankit(process &targ,int &reqproc, vector<process> &plistarg,vector<int> &availarg,vector<vector <int> > &needarg, vector<vector <int> > &alloarg, vector< vector <int> > &prequestarg  ){
for (int k = 0; k < plistarg.size(); k++){
    for( int q = 0; q < availarg.size(); q++){
        prequestarg[k][q] = 0;
    }
}
reqproc = targ.returnact().nnum - 1;
prequestarg[targ.returnact().nnum - 1][targ.returnact().restype - 1] = targ.returnact().resn;
for(int i = 0; i < availarg.size(); i ++){
        //loops to see if resource can be allocated and aborts + release resource of task if allocation doesn't work
    if(needarg[reqproc][i] - prequestarg[reqproc][i] >= 0){
            availarg[i] -= prequestarg[reqproc][i];
            alloarg[reqproc][i] += prequestarg[reqproc][i];
            needarg[reqproc][i] -= prequestarg[reqproc][i];

    }else{
            availarg[i] = availarg[i] + prequestarg[reqproc][i];
            alloarg[reqproc][i] = alloarg[reqproc][i] - prequestarg[reqproc][i];
            needarg[reqproc][i] = needarg[reqproc][i] + prequestarg[reqproc][i];
            targ.aborted = true;
            targ.instpointer = targ.actlist.size() - 1;
            targ.blockednum = 0;
            return true;

    }
}

 vector <int> tryit;
//tries hypothetical situation to see if it can be finished, checks the list process that are finished and the ones
//that need less than what is a available will be put in a loop to check if the processes can finsih
    for(int i = 0; i < availarg.size();i++){
        tryit.push_back(0);
    }
    vector <bool> finish;


    for(int i = 0; i < plistarg.size(); i++){
        if(!(plistarg[i].fined()) || plistarg[i].aborted == false){
            finish.push_back(false);
        }else{
            finish.push_back(true);
        }

    }
    for(int i = 0; i < availarg.size(); i++){
        tryit[i] = availarg[i];
    }

    int i = 0;
    //Loops and checks to see if there is a combination that can finish resulting in safe state for processes that need less
    //than what is available. If a situation comes to where the process needs more than what is available the loop for that
    //situation is stopped
    do{
        bool check = true;
        bool cyc = false;
        for(int j =0; j< availarg.size(); j ++){
            if(needarg[i][j] - tryit[j] > 0){
                check = false;
                break;
            }
        }
        if(finish[i] == false && check){
            for(int j = 0; j < availarg.size(); j++){
                tryit[j] += alloarg[i][j];
            }
            finish[i] = true;
            cyc = true;
        }
        i++;
        if(cyc){
            i = 0;
        }
    }while( i < plistarg.size());

    i = 0;
    //Checks if all the processes can be finished and if they can all be finished then it is in a safe state
    for(bool tfin = finish[i]; tfin == true; i++){
    if( i == (plistarg.size() - 1)){
        targ.instpointer++;
        for(int i = 0 ; i< availarg.size(); i++){
            prequestarg[reqproc][i] = 0;
        }
     targ.indelay = false;
    return true;
    //in a safe state so move to next instruction
        }

    }
    for(int i = 0; i < availarg.size(); i++){
        availarg[i] += prequestarg[reqproc][i];
        alloarg[reqproc][i] -= prequestarg[reqproc][i];
        needarg[reqproc][i] += prequestarg[reqproc][i];
    }
    for(int i = 0; i < availarg.size(); i ++){
        prequestarg[reqproc][i] = 0;
    }
    targ.blockednum ++;
    return false;
    //It is not safe so release the resources


}
